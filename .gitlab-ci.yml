image: docker:24

services:
    - docker:24-dind

stages:
    - security
    - build
    - test
    - release

variables:
    # Docker specifics
    DOCKER_BUILDKIT: 1
    DOCKER_HOST: tcp://docker:2376
    DOCKER_TLS_CERTDIR: "/certs"
    DOCKER_DRIVER: overlay2

    # image names
    IMAGE_BRANCH_MAIN: "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}"
    IMAGE_BRANCH_TEST: "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}-test"
    IMAGE_BRANCH_DEV: "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}-dev"
    IMAGE_RELEASE: "${CI_REGISTRY_IMAGE}:latest"
    IMAGE_CACHE: "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}-buildcache"

build-main:
    stage: build
    before_script:
        - echo "${CI_REGISTRY_PASSWORD}" | docker login "${CI_REGISTRY}" --username "${CI_REGISTRY_USER}" --password-stdin
    script:
        - docker context create builder
        - docker buildx create builder --use
        - docker buildx build --push --cache-to="type=registry,ref=${IMAGE_CACHE}-main" --cache-from="type=registry,ref=${IMAGE_CACHE}-main" --build-arg="GROUP=main" -t "${IMAGE_BRANCH_MAIN}" "."

build-test:
    stage: build
    before_script:
        - echo "${CI_REGISTRY_PASSWORD}" | docker login "${CI_REGISTRY}" --username "${CI_REGISTRY_USER}" --password-stdin
    script:
        - docker context create builder
        - docker buildx create builder --use
        - docker buildx build --push --cache-to="type=registry,ref=${IMAGE_CACHE}-test" --cache-from="type=registry,ref=${IMAGE_CACHE}-test" --build-arg="GROUP=test" -t "${IMAGE_BRANCH_TEST}" "."

build-dev:
    stage: build
    before_script:
        - export DOCKER_BUILDKIT=1
        - echo "${CI_REGISTRY_PASSWORD}" | docker login "${CI_REGISTRY}" --username "${CI_REGISTRY_USER}" --password-stdin
    script:
        - docker context create builder
        - docker buildx create builder --use
        - docker buildx build --push --cache-to="type=registry,ref=${IMAGE_CACHE}-dev" --cache-from="type=registry,ref=${IMAGE_CACHE}-dev" --build-arg="GROUP=dev" -t "${IMAGE_BRANCH_DEV}" "."

pytest:
    stage: test
    before_script:
        - export DOCKER_BUILDKIT=1
        - echo "${CI_REGISTRY_PASSWORD}" | docker login "${CI_REGISTRY}" --username "${CI_REGISTRY_USER}" --password-stdin
    script:
        - docker pull "${IMAGE_BRANCH_TEST}"
        - docker run --name="${CI_COMMIT_REF_SLUG}-pytest" "${IMAGE_BRANCH_TEST}" bash -c "python -m coverage run -m pytest && python -m coverage report && python -m coverage xml"
        - docker cp "${CI_COMMIT_REF_SLUG}-pytest:/app/coverage.xml" coverage.xml
    coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
    artifacts:
        reports:
            coverage_report:
                coverage_format: cobertura
                path: coverage.xml

ruff:
    stage: test
    before_script:
        - export DOCKER_BUILDKIT=1
        - echo "${CI_REGISTRY_PASSWORD}" | docker login "${CI_REGISTRY}" --username "${CI_REGISTRY_USER}" --password-stdin
    script:
        - docker pull "${IMAGE_BRANCH_DEV}"
        - docker run --name="${CI_COMMIT_REF_SLUG}-ruff" "${IMAGE_BRANCH_DEV}" bash -c "python -m ruff check --exit-zero --output-format gitlab . > gl-code-quality-report.json"
        - docker cp "${CI_COMMIT_REF_SLUG}-ruff:/app/gl-code-quality-report.json" gl-code-quality-report.json
    artifacts:
        reports:
            codequality: ["gl-code-quality-report.json"]

mypy:
    stage: test
    before_script:
        - export DOCKER_BUILDKIT=1
        - echo "${CI_REGISTRY_PASSWORD}" | docker login "${CI_REGISTRY}" --username "${CI_REGISTRY_USER}" --password-stdin
    script:
        - docker pull "${IMAGE_BRANCH_DEV}"
        - docker run --name="${CI_COMMIT_REF_SLUG}-mypy" "${IMAGE_BRANCH_DEV}" bash -c "python -m mypy . --cobertura-xml-report ."
        - docker cp "${CI_COMMIT_REF_SLUG}-mypy:/app/cobertura.xml" mypy.xml
    artifacts:
        reports:
            coverage_report:
                coverage_format: cobertura
                path: mypy.xml

release-docker:
    stage: release
    rules:
        - if: $CI_COMMIT_TAG
    before_script:
        - export DOCKER_BUILDKIT=1
        - echo "${CI_REGISTRY_PASSWORD}" | docker login "${CI_REGISTRY}" --username "${CI_REGISTRY_USER}" --password-stdin
    script:
        - docker pull "${IMAGE_BRANCH_MAIN}"
        - docker tag "${IMAGE_BRANCH_MAIN}" "${IMAGE_RELEASE}"
        - docker push "${IMAGE_RELEASE}"

release-tag:
    stage: release
    image: registry.gitlab.com/gitlab-org/release-cli:latest
    rules:
        - if: $CI_COMMIT_TAG
    script:
        - echo "Create Release"
    release:
        name: "Release ${CI_COMMIT_TAG}"
        description: "Release of ${CI_COMMIT_TAG}"
        tag_name: "${CI_COMMIT_TAG}"
        ref: "${CI_COMMIT_TAG}"

include:
    - template: Security/SAST.gitlab-ci.yml
    - template: Security/Container-Scanning.gitlab-ci.yml
    - template: Security/Dependency-Scanning.gitlab-ci.yml

sast:
    stage: security

gemnasium-dependency_scanning:
    stage: security

container_scanning:
    variables:
        CS_IMAGE: "${IMAGE_BRANCH_MAIN}"
