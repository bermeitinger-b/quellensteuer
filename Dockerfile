# syntax=docker/dockerfile:1.4
FROM python:3.13-slim as base
SHELL ["/bin/bash", "-e", "-u", "-o", "pipefail", "-c"]

ENV USER_NAME="quellensteuer"
ENV USER_ID="1000"
ENV USER_HOME="/home/${USER_NAME}"
ENV APP_ROOT="/app"
ENV UV_CACHE_DIR="${USER_HOME}/.cache/uv"

ENV PYTHONUNBUFFERED="1"
ENV PYTHONDONTWRITEBYTECODE="1"
ENV PYTHONIOENCODING="UTF-8"
ENV UV_COMPILE_BYTECODE=1
ENV UV_LINK_MODE=copy

RUN \
  --mount=type=cache,sharing=locked,target=/var/cache/apt \
  --mount=type=cache,sharing=locked,target=/var/lib/apt \
  <<EOF

  # Apt setup
  export DEBIAN_FRONTEND="noninteractive"
  echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
  rm -r /etc/apt/apt.conf.d/docker-clean
  echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache

  # Install base dependencies
  apt-get update -q
  apt-get upgrade --yes
  apt-get install --yes --no-install-recommends \
    "build-essential" \
    "gosu"

  useradd \
    --create-home \
    --no-log-init \
    --shell "/bin/bash" \
    --uid "${USER_ID}" \
    "${USER_NAME}"

  mkdir -p "${APP_ROOT}"
  mkdir -p "${APP_ROOT}/.venv"
  chown -R "${USER_NAME}:${USER_NAME}" "${APP_ROOT}"

  mkdir -p "${UV_CACHE_DIR}"
  chown -R "${USER_NAME}:${USER_NAME}" "${USER_HOME}"

  # Done.
EOF

# Install uv via their docker image
COPY --from=ghcr.io/astral-sh/uv:latest /uv /uvx /bin/

USER "${USER_NAME}"
WORKDIR "${APP_ROOT}"
COPY --chown="${USER_ID}" . "${APP_ROOT}"

ARG GROUP="main"
RUN \
  --mount=type=cache,uid="${USER_ID}",target="${UV_CACHE_DIR}" \
  <<EOF

  uv_venv=(
    "uv"
    "venv"
    "--python" "/usr/local/bin/python"
  )
  "${uv_venv[@]}"

  uv_cmd=(
    "uv"
    "sync"
    "--frozen"
    "--compile-bytecode"
  )

  if [[ "${GROUP}" == "main" ]]; then
    uv_cmd+=(  )
  elif [[ "${GROUP}" == "test" ]]; then
    uv_cmd+=( "--group" "test" )
  elif [[ "${GROUP}" == "dev" ]]; then
    uv_cmd+=( "--group" "dev" "--group" "test" )
  else
    echo "Please give which group you want to install: 'main', 'dev', or 'test'"
    exit 1
  fi

  # Use uv to install project
  "${uv_cmd[@]}"

  # Done.
EOF

ENV PATH="${APP_ROOT}/.venv/bin:$PATH"

CMD ["python", "-m", "uvicorn", "quellensteuer:app", "--host", "0.0.0.0", "--port", "8000"]
