import io
from collections import defaultdict
from decimal import Decimal
from pathlib import Path
from typing import Any

from fastapi import FastAPI, HTTPException, status
from fastapi.middleware.cors import CORSMiddleware
from zstandard import ZstdDecompressor

__FILES = {
    2018: Path(__file__).parent.parent.joinpath("data", "tar18sg.txt.zst"),
    2019: Path(__file__).parent.parent.joinpath("data", "tar19sg.txt.zst"),
    2020: Path(__file__).parent.parent.joinpath("data", "tar20sg.txt.zst"),
    2021: Path(__file__).parent.parent.joinpath("data", "tar21sg.txt.zst"),
    2022: Path(__file__).parent.parent.joinpath("data", "tar22sg.txt.zst"),
    2023: Path(__file__).parent.parent.joinpath("data", "tar23sg.txt.zst"),
    2024: Path(__file__).parent.parent.joinpath("data", "tar24sg.txt.zst"),
    2025: Path(__file__).parent.parent.joinpath("data", "tar25sg.txt.zst"),
}

__RULESET: dict[str, Any] = {
    "skip_leading_lines": 2,
    "break_here_indicator": "99",
    "fields": {
        "mode": {"min": 6, "max": 9, "type": str, "convert": lambda x: x},
        "threshold": {
            "min": 25,
            "max": 33,
            "type": Decimal,
            "convert": lambda x: x / 100,
        },
        "tariff": {"min": 55, "max": 59, "type": Decimal, "convert": lambda x: x / 100},
    },
}

MAX_BRUTTO = 1_000_000


def _parse_file(path: Path) -> dict[str, list[tuple[Decimal, Decimal]]]:
    tariffs: dict[str, list[tuple[Decimal, Decimal]]] = defaultdict(list)
    with path.open("rb") as f:
        dctx = ZstdDecompressor()
        stream_reader = dctx.stream_reader(f)
        text_stream = io.TextIOWrapper(stream_reader, encoding="utf-8")
        for _ in range(__RULESET["skip_leading_lines"] - 1):
            next(text_stream)
        for line in text_stream:
            if line.startswith(__RULESET["break_here_indicator"]):
                break
            fields = {}
            for field, limits in __RULESET["fields"].items():
                fields[field] = line[limits["min"] : limits["max"]]

            tariffs[fields["mode"]].append(
                (
                    __RULESET["fields"]["threshold"]["convert"](
                        __RULESET["fields"]["threshold"]["type"](fields["threshold"])
                    ),
                    __RULESET["fields"]["tariff"]["convert"](__RULESET["fields"]["tariff"]["type"](fields["tariff"])),
                )
            )

    # reverse sort by threshold for easier access later
    for mode, tariff in tariffs.items():
        tariffs[mode] = sorted(tariff, key=lambda x: x[0], reverse=True)

    return tariffs


__TARIFFS = {year: _parse_file(Path(file)) for year, file in __FILES.items()}

app = FastAPI(title=f"{__name__}_app")

app.add_middleware(CORSMiddleware, allow_origins=["*"], allow_methods=["GET"], allow_headers=["*"])


@app.get("/value", response_model=float)
async def get_value(year: int, mode: str, brutto: Decimal) -> float:
    tariff = await get_tariff(year, mode, brutto)
    return float(brutto * (Decimal(tariff) / Decimal(100)))


@app.get("/tariff", response_model=float)
async def get_tariff(year: int, mode: str, brutto: Decimal) -> float:
    """Get the tariff for a given year, mode and brutto income."""
    # locate tariff

    if year not in __TARIFFS:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="year not supported")
    if mode not in __TARIFFS[year]:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="mode not supported")
    if brutto < 1:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="brutto too low")
    if brutto > MAX_BRUTTO:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="brutto too high")

    for threshold, tariff in __TARIFFS[year][mode]:
        if brutto >= threshold:
            return float(tariff)

    # This will never be reached but coverage will complain here. It's good!
    raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST, detail="no tariff found")
