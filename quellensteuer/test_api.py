from http import HTTPStatus

import pytest
from fastapi.testclient import TestClient


@pytest.fixture
def client() -> TestClient:
    from quellensteuer.api import app

    return TestClient(app)


def test_tariff_2018_b0n_10000(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2018", "mode": "B0N", "brutto": "10000"})
    assert response.status_code == HTTPStatus.OK
    assert response.text == "11.27"


def test_tariff_2020_b1y_67500(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2020", "mode": "B1Y", "brutto": "67500"})
    assert response.status_code == HTTPStatus.OK
    assert response.text == "27.91"


def test_tariff_2020_b1y_67501(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2020", "mode": "B1Y", "brutto": "67501"})
    assert response.status_code == HTTPStatus.OK
    assert response.text == "27.94"


def test_tariff_2020_b4n_125001(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2020", "mode": "B4N", "brutto": "125001"})
    assert response.status_code == HTTPStatus.OK
    assert response.text == "26.61"


def test_tariff_2021_b4n_125001(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2021", "mode": "B4N", "brutto": "125001"})
    assert response.status_code == HTTPStatus.OK
    assert response.text == "26.32"


def test_tariff_2025_b2n_125001(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2025", "mode": "B2N", "brutto": "125001"})
    assert response.status_code == HTTPStatus.OK
    assert response.text == "25.6"


def test_tariff_illegal_year(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2005", "mode": "B3N", "brutto": "5000"})
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert response.json()["detail"] == "year not supported"


def test_tariff_illegal_mode(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2022", "mode": "XXX", "brutto": "5000"})
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert response.json()["detail"] == "mode not supported"


def test_tariff_illegal_brutto_low(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2020", "mode": "B1Y", "brutto": "0"})
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert response.json()["detail"] == "brutto too low"


def test_tariff_illegal_brutto_high(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2020", "mode": "B1Y", "brutto": "10_000_000"})
    assert response.status_code == HTTPStatus.BAD_REQUEST
    assert response.json()["detail"] == "brutto too high"


def test_tariff_empty_request(client: TestClient) -> None:
    response = client.get("/tariff", params={})
    assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
    r = response.json()["detail"]
    assert r[0] == {
        "input": None,
        "loc": ["query", "year"],
        "msg": "Field required",
        "type": "missing",
    }
    assert r[1] == {
        "input": None,
        "loc": ["query", "mode"],
        "msg": "Field required",
        "type": "missing",
    }
    assert r[2] == {
        "input": None,
        "loc": ["query", "brutto"],
        "msg": "Field required",
        "type": "missing",
    }


def test_tariff_missing_year(client: TestClient) -> None:
    response = client.get("/tariff", params={"mode": "B1Y", "brutto": "10000"})
    assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
    r = response.json()["detail"]
    assert r[0] == {
        "input": None,
        "loc": ["query", "year"],
        "msg": "Field required",
        "type": "missing",
    }


def test_tariff_missing_mode(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2020", "brutto": "10000"})
    assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
    r = response.json()["detail"]
    assert r[0] == {
        "input": None,
        "loc": ["query", "mode"],
        "msg": "Field required",
        "type": "missing",
    }


def test_tariff_missing_brutto(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2020", "mode": "B1Y"})
    assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
    r = response.json()["detail"]
    assert r[0] == {
        "input": None,
        "loc": ["query", "brutto"],
        "msg": "Field required",
        "type": "missing",
    }


def test_tariff_year_no_int(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "XXXX", "mode": "B1Y", "brutto": "10000"})
    assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
    r = response.json()["detail"]
    assert r[0] == {
        "input": "XXXX",
        "loc": ["query", "year"],
        "msg": "Input should be a valid integer, unable to parse string as an integer",
        "type": "int_parsing",
    }


def test_tariff_brutto_no_decimal(client: TestClient) -> None:
    response = client.get("/tariff", params={"year": "2021", "mode": "B2N", "brutto": "XXXX"})
    assert response.status_code == HTTPStatus.UNPROCESSABLE_ENTITY
    r = response.json()["detail"]
    assert r[0] == {
        "input": "XXXX",
        "loc": ["query", "brutto"],
        "msg": "Input should be a valid decimal",
        "type": "decimal_parsing",
    }


def test_value(client: TestClient) -> None:
    response = client.get("/value", params={"year": "2021", "mode": "B2N", "brutto": "7560"})
    assert response.status_code == HTTPStatus.OK
    assert response.text == "272.16"
