# Quellensteuer
An API that returns the percentage of your individual Quellensteuer rate.

It automates looking up your individual rate from the very long and unreadable PDFs: https://www.sg.ch/steuern-finanzen/steuern/formulare-wegleitungen/quellensteuern.html

Currently, this API only works with the Kanton **St.Gallen**.
Other Kantons can be supported (relatively) easily because the source files have the same structure but I don't need that now. Also, some Kantons have a fixed rate for specific brutto values, this is not implemented in this API (because SG doesn't have this fixed rate).

The only supported years are 2018-2025 although it is very easy to extend it further.
I don't need that much history data, so I didn't check these.

The files in the `data/` folder are from: [https://www.estv.admin.ch/estv/de/home/direkte-bundessteuer/dbst-quellensteuer/qst-tarife-loehne.html](https://www.estv.admin.ch/estv/de/home/direkte-bundessteuer/dbst-quellensteuer/qst-tarife-loehne.html)

## Dependencies
Development is done with Python 3.13. There is no testing of any other versions.
(No >=3.10 features are used at the moment as far as I know, so 3.10 should still be fine, although uv will complain.)

The API is build on `FastAPI` [Documentation](https://fastapi.tiangolo.com/).
The server is `uvicorn` [Documentation](https://www.uvicorn.org).

Install the requirements with

`uv sync`


## Running
You can either run it locally with

```bash
uv run uvicorn quellensteuer:app
```

or use the provided docker image:

```bash
docker run -p 8000:8000 registry.gitlab.com/bermeitinger-b/quellensteuer:latest
```

- Select a port convenient to you.
- Add `-d` to run as daemon.
- Add `--restart=always` to automatically start the container.
- Use a reverse proxy as TLS proxy (like _nginx_ or _caddy_)


## Usage
The API has one `GET` endpoint: `/tariff`
It takes exactly 3 arguments:

- `year`: Which year to operate on. Possible values: `2018`, `2019`, `2020`, `2021`, `2022`, `2023`.
- `mode`: Your personal tax mode (Example: `B1N` => Married, single earner, one child, no church tax)
- `brutto`: Floating number of your personal brutto salary.


## Examples
(Using `httpie` as example, works with curl or wget as well)

```sh
http GET :8000/tariff year==2018 mode==B0N brutto==4604.50
4.49
```

```sh
http GET :8000/tariff year==2019 mode==B0N brutto==4604.50
4.42
```

```sh
http GET :8000/tariff year==2020 mode=B0N brutto==4604.50
4.37
```

```sh
http GET :8000/tariff year==2020 mode=B2Y brutto==5800
1.14
```

The API will return a JSON error message if there is an error in the requests.
If there is no rate set to your input, the result will be 'Tariff not found'. (E.g. when `mode=='B10Y'`, which is not support by the tax input files.)


## Include in Google Sheets
Especially cool and the main driver of this API is the possibility to use this API in Google Sheets (maybe Excel but I don't care). First, you must have a publically available server. Assume it is at `https://example.com/tariff`, then you can use in your cell:

`=IMPORTDATA(CONCAT("https://example.com/tariff?year=", YEAR(E1), "&mode=", E31, "&brutto=", E19)) / 100`

This is specific for my sheet, in cell _E1_ is the date, _E31_ contains the mode, and the brutto value is in _E19_.
Also note, that the API returns the percentage value, so you might have to divide it by 100 to get the real value.

## Privacy
There is no public instance, you have to run it yourself. You can see in the code that there is no telemetry.
Be careful if you have a public instance to use TLS and turn off any request data logging
